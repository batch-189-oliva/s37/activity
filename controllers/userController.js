const User = require('../models/User.js');
const Course = require('../models/Course.js')
const bcrypt = require('bcrypt');
const auth = require('../auth.js')

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


// User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) 
		//hashSync(<dataToBeHash>, <saltRound>)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			// compareSync(<data>, <encryptedPassword>)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.id).then(result => {
		if(result == null){

			return false
		} else {	
			 
			result.password = ""
			return result		
		}
	})
}

//Enroll User to a Class
module.exports.enroll = async (data) => {

if (data.IsAdmin == false) {

		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.enrollments.push({courseId: data.courseId});

			return user.save().then((user, error) => {

				if (error){

					return false
				} else {

					return true
				}
			})
		})

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

			course.enrollees.push({userId: data.userId});

			return course.save().then((course, error) => {

				if (error) {

					return false
				} else {

					return true
				}
			})
		})

		if (isUserUpdated && isCourseUpdated) {
			return true

		} else {

			return false
		}

} else {

	return 'User must not be an Admin'
}
	
}