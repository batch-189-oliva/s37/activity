const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController')
const auth = require('../auth')

// Route for creating a course 
router.post("/", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin){
		courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController))
	} else {
		response.send('User must be Admin!')
	}
})

// Route for retriving all the courses
router.get("/all", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => response.send(resultFromController))

})

// Route for retriving all active courses
router.get("/", (request, response) => {

	courseController.getAllActive().then(resultFromController => response.send(resultFromController))
})

// Route for retrieving a specific course
router.get("/:courseId", (request, response) => {

	console.log(request.params)

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (request, response) => {

	courseController.updateCourse(request.params, request.body).then(resultFromController => response.send(resultFromController))
})


// Route for updating a course
router.put("/:courseId/archive", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		
		courseController.archiveCourse(request.params, request.body).then(resultFromController => response.send(resultFromController))
	} else {
		response.send('User must be Admin!')
	}
	
})

module.exports = router;