const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes.js')
const courseRoutes = require('./routes/courseRoutes.js')
// Allows our backend applicaiton to be available to our frontend applicaiton
const cors = require('cors');
const port = 4000;

const app = express();

// Allows all resources to access our backend application
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/users', userRoutes)
app.use('/courses', courseRoutes)

mongoose.connect(`mongodb+srv://admin123:admin123@zuitt-bootcamp.sr40tbb.mongodb.net/s37-s41?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})